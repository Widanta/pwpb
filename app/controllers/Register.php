<?php

class Register extends Controller {
    public function index()
    {
        if(!isset($_SESSION['user']))
        {
            $data['judul'] = 'Register';

            $this->view('templates/header', $data);
            $this->view('Register/index', $data);
            $this->view('templates/footer');
        }
        else 
        {
            header('Location: ' . BASE_URL);
            exit;
        }
    }

    public function sign()
    {
        $userData = [
            'nama_user' => $_POST['nama_user'],
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'password' => md5($_POST['password']),
        ];

        $userData['password'] = md5($_POST['password']);
        // $salt = $this->salt(16);
        $userData['password'] .= SALT;

        // $test = substr(md5(time()),0,15);
        // var_dump($test);

        if($this->model('User_model')->validateUser($userData) < 1)
        {
            if($this->model('User_model')->addUser($userData) > 0)
            {
                header('Location: ' . BASE_URL . '/login');
                exit;
            }
        }

        header('Location: ' . BASE_URL . '/register');
        exit;

    }
}