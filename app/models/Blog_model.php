<?php
class Blog_model
{
    private $db;
    private $table = 'blog';

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllBlogData()
    {
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function getBlogById($id)
    {
        $this->db->query("SELECT * FROM {$this->table} WHERE id_blog = :id_blog");
        $this->db->bind('id_blog', $id);
        return $this->db->single();
    }

    public function getBlogJoinUserById($id)
    {
        $this->db->query("SELECT * FROM blog LEFT JOIN users ON blog.id_user = users.id_user WHERE id_blog = :id_blog");
        $this->db->bind('id_blog', $id);
        return $this->db->single();
    }


    public function addBlogData($data)
    {
        $query = "INSERT INTO {$this->table} VALUES 
        (null,:id_user,:judul,:tulisan)";
        $this->db->query($query);
        $this->db->bind('id_user', $data['id_user']);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dataBlogAndUserJoin()
    {
        $query = "SELECT * FROM blog LEFT JOIN users ON blog.id_user = users.id_user ORDER BY id_blog ASC";
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function editBlogData($data)
    {
        $query = "UPDATE {$this->table} SET
						id_user = :id_user,
                        judul = :judul,
						tulisan = :tulisan
						WHERE id_blog = :id_blog";
        $this->db->query($query);
        $this->db->bind('id_blog', $data['id_blog']);
        $this->db->bind('id_user', $data['id_user']);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteBlogData($id)
    {
        $this->db->query("DELETE FROM {$this->table} WHERE id_blog = :id_blog");
        $this->db->bind('id_blog', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }
}
