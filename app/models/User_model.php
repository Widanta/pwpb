<?php

class User_model{

    private $table = "users";
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllUserData()
    {
        $this->db->query("SELECT * FROM " . $this->table);
        return $this->db->resultSet();
    }

    public function getUserById($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id_user = :id_user");
        $this->db->bind('id_user', $id);
        return $this->db->single();
    }

    public function getUserByUname($userData)
    {
        $query = 'SELECT * FROM users WHERE username = :username AND password = :password';
        $this->db->query($query);
        $this->db->bind('username', $userData['username']);
        $this->db->bind('password', $userData['password']);
        return $this->db->single();
    }

    public function addUser($userData)
    {
        $query = 'INSERT INTO users(id_user, nama_user, username, email, password) VALUES (null, :nama_user,:username, :email, :password)';
        $this->db->query($query);
        $this->db->bind('nama_user', $userData['nama_user']);
        $this->db->bind('username', $userData['username']);
        $this->db->bind('email', $userData['email']);
        $this->db->bind('password', $userData['password']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function validateUser($userData)
    {
        $query = 'SELECT * FROM users WHERE username = :username OR email = :email';
        $this->db->query($query);
        $this->db->bind('username', $userData['username']);
        $this->db->bind('email', $userData['email']);

        return $this->db->single();
    }
}