<div class="container">
    <div class="row justify-content-center">
        <div class="card-login">
            <div class="col-xl-4 card-box-login">
                <form action="<?= BASE_URL; ?>/login/sign" method="post">
                    <div class="mb-3">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                    <div class="mb-3">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-secondary float-end mt-2"> Kirim</button>
                </form>
            </div>
        </div>
    </div>
</div>
