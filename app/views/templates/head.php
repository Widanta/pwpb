<nav class="navbar navbar-expand-lg bg-light fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/index">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/user">User</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL;?>/blog">Blog</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL;?>/logout">Keluar</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

    <section id="dashboard-layouts">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xxl-3 col-xl-3 col-md-3">
            <button id="btn-mobile-sidebar">Menu Dashboard</button>
            <section id="sidebar">
              <div class="sidebar-list">
                <h4>Main</h4>
                <ul class="sidebar-item">
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fas fa-cog"></i>
                      Dashboard
                    </a>
                  </li>
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fas fa-cog"></i>
                      Ubah Profil
                    </a>
                  </li>
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fas fa-cog"></i>
                      Ubah Kata Sandi
                    </a>
                  </li>
                  <li>
                    <hr />
                  </li>
                </ul>
              </div>
              <div class="sidebar-list">
                <h4>Blog</h4>
                <ul class="sidebar-item">
                  <li class="sidebar-link">
                    <a href="">
                    <i class="fas fa-pencil-alt"></i>
                      Buat Blog
                    </a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-xxl-9 col-xl-9 col-md-9">
            <section id="main-content">