<nav class="navbar navbar-expand-lg bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/index">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/user">User</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL;?>/blog">Blog</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
    <div class="row justify-content-center text-center">
        <div class="col-xl-6">
            <img src="<?= BASE_URL; ?>/img/lilgru.jpg" alt="" width="150" class="rounded-circle img-fluid">
            <h3><?= $data['nama']; ?></h3>
            <p>Front-End Developer | Pelajar</p>
        </div>
    </div>
</div>