<div class="row justify-content-center">
  <div class="col">
    <h3>Selamat datang, <span class="name-user text-capitalize"><?= $_SESSION['user']['username']; ?></span></h3>
  </div>
</div>
<div class="row justify-content-center card-container">
  <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
    <div class="card card-list-container">
      <a href="" class="text-decoration-none text-dark">
        <div class="row justify-content-center align-items-center">
          <div class="col-9">
            <div class="card-body">
              <h2 class="card-title">0</h5>
              <p class="card-text">Total User</p>
            </div>
          </div>
          <div class="col-3">
            <div class="card-icon">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </a>
      <div class="bootom-card">
        <a href="">
          <p>Selengkapnya <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
        </a>
      </div>
    </div>
  </div>
  <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
    <div class="card card-list-container">
      <a href="" class="text-decoration-none text-dark">
        <div class="row justify-content-center align-items-center">
          <div class="col-9">
            <div class="card-body">
              <h2 class="card-title">0</h5>
              <p class="card-text">Total Blog</p>
            </div>
          </div>
          <div class="col-3">
            <div class="card-icon">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </a>
      <div class="bootom-card">
        <a href="">
          <p>Selengkapnya <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
        </a>
      </div>
    </div>
  </div>
  </div>
</div>